# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.flatpages.views import flatpage
from django.views.generic import TemplateView
from django.views import defaults as default_views
from django.contrib import flatpages

from flashlight.main import views

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='pages/home.html'), name='home'),
    url(r'^video/$', TemplateView.as_view(template_name='pages/video.html'), name='video'),
    url(r'^order/$', views.OrderView.as_view(template_name='pages/order.html'), name='order'),
    url(r'^batteries/$', views.BattariesView.as_view(template_name='pages/batteries.html'), name='batteries'),
    url(r'^warranty/$', views.WarrantyView.as_view(template_name='pages/warranty.html'), name='warranty'),
    url(r'^car_charger/$', views.CarChargerView.as_view(template_name='pages/car_charger.html'), name='car_charger'),
    url(r'^thankyou/$', views.ThankYouView.as_view(template_name='pages/thankyou.html'), name='thankyou'),
    url(r'^page-privacy/$', flatpage, {'url': '/page-privacy/'}, name='privacy'),
    url(r'^page-contact/$', flatpage, {'url': '/page-contact/'}, name='contact'),
    url(r'^page-terms/$', flatpage, {'url': '/page-terms/'}, name='terms'),

    # url(r'^about/$', TemplateView.as_view(template_name='pages/about.html'), name='about'),
    # url(r'')
    # Django Admin, use {% url 'admin:index' %}
    url(settings.ADMIN_URL, admin.site.urls),
    url(r'^(?P<url>.*/)$', flatpage),
    # User management

    # Your stuff: custom urls includes go here

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        url(r'^400/$', default_views.bad_request, kwargs={'exception': Exception('Bad Request!')}),
        url(r'^403/$', default_views.permission_denied, kwargs={'exception': Exception('Permission Denied')}),
        url(r'^404/$', default_views.page_not_found, kwargs={'exception': Exception('Page not Found')}),
        url(r'^500/$', default_views.server_error),
    ]
    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns += [
            url(r'^__debug__/', include(debug_toolbar.urls)),
        ]
