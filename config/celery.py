# # from __future__ import absolute_import
# #
# # import os
# #
# # from celery import Celery
# #
# # # set the default Django settings module for the 'celery' program.
# # os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings.local')
# #
# # from django.conf import settings  # noqa
# #
# # app = Celery('proj')
# #
# # # Using a string here means the worker will not have to
# # # pickle the object when using Windows.
# # app.config_from_object('django.conf:settings')
# # app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
# # app.conf.update(
# #     CELERY_RESULT_BACKEND='djcelery.backends.database:DatabaseBackend',
# # )
# #
# # @app.task(bind=True)
# # def debug_task(self):
# #     print('Request: {0!r}'.format(self.request))
#
#
from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from django.conf import settings  # noqa

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings.local')

app = Celery('flashlight')

# Using a string here means the worker don't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
# app.conf.update(
#     CELERY_RESULT_BACKEND='djcelery.backends.database:DatabaseBackend',
#     RESULT_BACKEND='djcelery.backends.database:DatabaseBackend',
# )

@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
