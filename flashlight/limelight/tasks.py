# -*- coding: utf8 -*-
import datetime
import traceback
from pprint import pprint
from urllib.parse import parse_qs

from django.conf import settings
import requests

from config.celery import app
from flashlight.limelight.models import CrmLogs

__author__ = 'Derbichev Mikhail, arhangel662@gmail.com'


@app.task
def test_task():
    print('working, %s' % datetime.datetime.now())


@app.task
def call(data, ttype='transact', log_id=None):
    # importing here, instead of module top, to avoid circular import
    result = {'is_error': False, 'res': {}}
    url = "https://oneclickawaytgc.limelightcrm.com/admin/%s.php" % ttype
    base_data = {"username": settings.LL.get('login'),
                 "password": settings.LL.get('password'),
                 "campaignId": settings.LL.get('campaign')}

    data.update(base_data)

    # save call to log
    save_data = data.copy()
    if 'password' in save_data:
        del save_data['password']
    if log_id:
        log = CrmLogs.objects.get(id=log_id)
        log.send_text = save_data
        log.save()
    else:
        log = CrmLogs.objects.create(command_type=data.get('method'), send_text=save_data)

    try:
        resp = requests.post(url, data=data)
        # print(71, resp.content)
        result['res'] = parse_qs(resp.content.decode('utf-8'))
        res2 = {}
        for key, val in result['res'].items():
            if val:
                res2[key] = val[0]
        result['res'] = res2

        if 'error_message' in result['res'] or 'errorMessage' in result['res']:
            result['is_error'] = True

        elif result['res'].get('errorFound', None) == ['1']:
            result['is_error'] = True
    except:
        result['is_error'] = True
        result['res'] = traceback.format_exc()
    save_dic = result['res']
    pprint(save_dic)
    log.get_text = save_dic
    log.is_error = result['is_error']
    log.save()
    log.create_order()
    return result
