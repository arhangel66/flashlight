from pprint import pprint

from django.conf import settings
from django.contrib.postgres.fields import JSONField
from django.db import models
from model_utils.models import TimeStampedModel
import json
import time



class CrmLogs(TimeStampedModel):
    crm_name = models.CharField(blank=True, max_length=30)
    command_type = models.CharField(null=True, blank=True, max_length=100)
    url = models.CharField(null=True, blank=True, max_length=255)
    # send_text = JSONField(null=True, blank=True)
    # get_text = JSONField(null=True, blank=True)
    send_text = models.TextField(null=True, blank=True)
    get_text = models.TextField(null=True, blank=True)
    task_id = models.CharField(max_length=255, blank=True, null=True)

    is_error = models.NullBooleanField()

    # is_real = models.BooleanField(default=True)

    def __str__(self):
        return u"%s - %s - %s" % (self.id, self.url, self.modified)

    def get_time(self):
        length = (self.modified - self.created)
        return "%s.%s sec" % (length.seconds, length.microseconds // 100000)

    def get_res(self):
        """
        return result_dict
        :return:
        """
        try:
            result = json.loads(self.get_text.replace("'", '"'))
        except:
            result = self.get_text
        return result

    def get_send(self):
        try:
            result = json.loads(self.send_text.replace("'", '"'))
        except:
            result = self.send_text
        return result

    def is_finish(self):
        # TODO make waiting finish 5-7 sec
        for i in range(7):
            log = CrmLogs.objects.get(id=self.id)
            print(i, log.get_text)
            if log.get_text is not None:
                self.get_text = log.get_text
                self.is_error = log.is_error
                return True
            time.sleep(1)

        self.is_error = True
        self.get_text = 'Too long response. Please try again later'
        self.save()
        return True

    def get_error_text(self):
        res = self.get_res()
        print(45, res)
        error_text = res.get('errorMessage') if 'errorMessage' in res else str(res)
        return error_text

    def get_order_id(self):
        res = self.get_res()
        return res.get('orderId')

    def create_order(self):
        if self.command_type in ['NewOrder', 'NewOrderCardOnFile'] and self.is_finish() and not self.is_error:
            if not Order.objects.filter(log=self.id).exists():
                res = self.get_res()
                send = self.get_send()

                test_card_number = '4640111111111111'
                if self.command_type == 'NewOrder':
                    order_dict = {
                        'name': "%s %s" % (send.get('firstName'), send.get('lastName')),
                        'product_id': send.get('productId'),
                        'product_name': settings.PRODUCTS_FOR_ADMIN.get(send.get('productId')),
                        'count': send.get('product_qty_%s' % send.get('productId')),
                        'order_total': res.get('orderTotal'),
                        'limelight_id': res.get('orderId'),
                        'log_id': self.id,
                        'is_real': True if send.get('creditCardNumber') != test_card_number else False,
                        'created': self.created
                    }
                elif self.command_type == 'NewOrderCardOnFile':
                    order_id = send.get('previousOrderId')
                    print(94, order_id)
                    order_dict = Order.objects.filter(limelight_id=order_id).values('name', 'is_real')
                    if not order_dict:
                        return ''
                    order_dict = order_dict[0]
                    order_dict.update({
                        'product_id': send.get('productId'),
                        'count': 1,
                        'product_name': settings.PRODUCTS_FOR_ADMIN.get(send.get('productId')),
                        'order_total': res.get('orderTotal'),
                        'limelight_id': res.get('orderId'),
                        'log_id': self.id,
                        'created': self.created
                    })
                pprint(order_dict)
                order = Order.objects.create(**order_dict)
                return order


class Order(models.Model):
    name = models.CharField(blank=True, null=True, max_length=50)
    product_id = models.CharField(blank=True, null=True, max_length=30)
    product_name = models.CharField(blank=True, null=True, max_length=60)
    count = models.IntegerField(blank=True, null=True)
    created = models.DateTimeField(blank=True, null=True)
    order_total = models.FloatField(blank=True, null=True, max_length=30)
    limelight_id = models.IntegerField(blank=True, null=True)
    log = models.ForeignKey(CrmLogs, blank=True, null=True)
    is_real = models.BooleanField(default=True)

    def __str__(self):
        return "%s - %s - %s" % (self.name, self.product_id, self.order_total)

    def get_ll_link(self):
        url = "https://oneclickawaytgc.limelightcrm.com/admin/orders.php?show_details=show_details&show_folder=" \
              "view_all&fromPost=1&act=&sequence=1&show_by_id=%s" % self.limelight_id
        return "<a href='%s' target='_blank'>%s</a>" % (url, self.limelight_id)
    get_ll_link.allow_tags = True
    get_ll_link.short_description = "LL order"

    def created_time(self):
        if self.created:
            return "%s" % self.created.strftime("%Y-%m-%d %H:%M")
        return ''
    created_time.short_description = 'Created'
