# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-12-30 12:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('limelight', '0005_order_created'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='product_name',
            field=models.CharField(blank=True, max_length=60, null=True),
        ),
    ]
