from daterange_filter.filter import DateRangeFilter
from django import forms
from django.contrib import admin
# from prettyjson import PrettyJSONWidget
from flashlight.limelight.models import CrmLogs, Order

from django.contrib.admin.views.main import ChangeList
from django.db.models import Sum, Avg


class TotalAveragesChangeList(ChangeList):
    #provide the list of fields that we need to calculate averages and totals
    fields_to_total = ['count', 'order_total', ]

    def get_total_values(self, queryset):
        """
        Get the totals
        """
        #basically the total parameter is an empty instance of the given model
        total =  Order()
        total.name = "Totals" #the label for the totals row
        for field in self.fields_to_total:

            setattr(total, field, round(list(queryset.aggregate(Sum(field)).items())[0][1], 2))
        return total


    def get_average_values(self, queryset):
        """
        Get the averages
        """
        average =  Order()
        average.custom_alias_name = "Averages" #the label for the averages row

        for field in self.fields_to_total:
            setattr(average, field, list(queryset.aggregate(Avg(field)).items())[0][1])
        return average


    def get_results(self, request):
        """
        The model admin gets queryset results from this method
        and then displays it in the template
        """
        super(TotalAveragesChangeList, self).get_results(request)
        #first get the totals from the current changelist
        print(45, dir(self))
        total = self.get_total_values(self.queryset)
        #then get the averages
        # average = self.get_average_values(self.queryset)
        #small hack. in order to get the objects loaded we need to call for
        #queryset results once so simple len function does it
        len(self.result_list)

        print(57, total)

        #and finally we add our custom rows to the resulting changelist
        self.result_list._result_cache.append(total)
        # self.result_list._result_cache.append(average)
# class JsonForm(forms.ModelForm):
#     class Meta:
#         model = CrmLogs
#         fields = '__all__'
#         widgets = {
#             'get_text': PrettyJSONWidget(),
#             'send_text': PrettyJSONWidget(),
#         }


@admin.register(CrmLogs)
class CrmLogsAdmin(admin.ModelAdmin):
    list_display = ('id', 'crm_name', 'url', 'is_error', 'created', 'get_time')
    list_filter = ('url', 'is_error',)

    # form = JsonForm


@admin.register(Order)
class CrmLogsAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'product_id', 'product_name', 'count', 'order_total', 'is_real', 'get_ll_link', 'created_time')
    list_filter = ('is_real', ('created', DateRangeFilter), )

    def get_changelist(self, request, **kwargs):
        return TotalAveragesChangeList
    # form = JsonForm
