from __future__ import print_function

import ast
import traceback
from datetime import datetime, timedelta
from pprint import pformat, pprint
from urllib.parse import parse_qs
from django.conf import settings
from django.contrib import messages

from flashlight.limelight import tasks
import requests

from flashlight.limelight.models import CrmLogs


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


class LimeLightApi(object):
    order_dict = {'total_amount': 0, 'products_info': {}}

    def __init__(self, crm=None):
        if crm:
            self.crm = crm

    def call(self, data, ttype='transact'):
        result = tasks.call(data, ttype)
        return result

    def get_orders(self, email='test@gmail.com', return_type=''):
        data = {"method": 'order_find',
                'criteria': 'email=%s' % email,
                'start_date': '06/08/2016',  # datetime.datetime.now().strftime("%m/%d/%Y"), #'06/08/2016',
                'end_date': '06/07/2020',
                'search_type': 'all',
                'campaign_id': self.crm.campaign,
                'return_type': return_type
                }
        resp = self.call(data)
        result = parse_qs(resp)
        return result

    def send_prospect(self, prospect_data):
        if not 'method' in prospect_data:
            prospect_data['method'] = 'NewProspect'
        result = self.call(prospect_data, 'transact')
        return result

    def get_prospect_dict(self, prospect_id):
        result = self.call({'method': 'prospect_view', 'prospect_id': prospect_id}, 'membership')
        if not result['is_error']:
            return result['res']
        else:
            return {}

    def update_prospect(self, prospect_id, new_prospect):
        valid_keys = ['first_name', 'last_name', 'address', 'city', 'state', 'zip', 'country', 'email ', 'phone',
                      'notes']

        changes_list = [['firstName', 'first_name'], ['lastName', 'last_name'], ['address1', 'address'],
                        ['zipcode', 'zip']]
        for pair in changes_list:
            if pair[0] in new_prospect:
                new_prospect[pair[1]] = new_prospect.pop(pair[0])

        # they want to get in update only NEW fields, so, we have to get current prospect and remove same fields
        # get current prospect
        old_prospect = self.get_prospect_dict(prospect_id)
        for key, val in new_prospect.items():
            if key in old_prospect and old_prospect[key] == new_prospect[key]:
                # print(key, 'is same')
                del new_prospect[key]

        prospect_data = {
            'method': 'prospect_update',
            'prospect_ids': '',
            'actions': '',
            'values': '',
        }
        # pprint(prospect_dict)
        for key, val in new_prospect.items():
            if key in valid_keys and val:
                prospect_data['prospect_ids'] += '%s,' % prospect_id
                prospect_data['actions'] += '%s,' % key
                prospect_data['values'] += '%s,' % val

        # remove last ','
        if prospect_data['actions']:
            prospect_data['actions'] = prospect_data['actions'][:-1]
            prospect_data['values'] = prospect_data['values'][:-1]
            prospect_data['prospect_ids'] = prospect_data['prospect_ids'][:-1]

        result = self.call(prospect_data, 'membership')
        # pprint(result)
        return result

    def prepare_order_dict(self, post, request=None, ip=None):
        product_id = str(post.get('purchase[product_id]'))
        product_price = None
        product_qty = None

        # calculate price, id and count
        if '_' in product_id:
            product_id, product_qty = product_id.split('_')
            product = settings.PRODUCTS.get(product_qty)
            product_price = product['price']

        # exception, when we have to change product_id and add to price extra_price/count
        if 'extra_product' in post:
            product_id = post['extra_product']['id']
            product_qty = int(product_qty)
            if product_qty > 0:
                product_price += round(post['extra_product']['price'] / product_qty, 2)
            else:
                product_price += post['extra_product']['price']

        payload = {
            'firstName': post.get('firstName', ''),
            'lastName': post.get('lastName', ''),
            'shippingAddress1': post.get('shippingAddress1', ''),
            'shippingAddress2': post.get('shippingAddress2', ''),
            'shippingCity': post.get('shippingCity', ''),
            'shippingState': post.get('shippingState', ''),
            'shippingZip': post.get('shippingZip', ''),
            'shippingCountry': 'US',
            'phone': post.get('phone', ''),
            'email': post.get('email', ''),
            'billingSameAsShipping': 'YES',
            'ipAddress': get_client_ip(request) if request else ip,
            'creditCardType': post.get('creditCardType', ''),
            'creditCardNumber': post.get('creditCardNumber', ''),
            'expirationDate': "%s%s" % (post.get('expmonth'), post.get('expyear')),
            'CVV': post.get('CVV', ''),
            'tranType': 'Sale',
            'productId': product_id,
            'shippingId': 1,
            'upsellProductIds': post.get('upsellProductIds', 1),
            'upsellCount': post.get('upsellCount', 0)
        }
        if product_price:
            # set price
            payload['dynamic_product_price_' + str(product_id)] = product_price

        if product_qty:
            payload['product_qty_' + str(product_id)] = product_qty

        return payload

    def send_order(self, payment_dict, request=None, ip=None):
        # prepare dict
        payload = self.prepare_order_dict(payment_dict, request, ip)

        if not 'method' in payload:
            payload['method'] = 'NewOrder'
        result = self.call(payload, 'transact')

        return result

    def send_order_async(self, payment_dict, request=None, ip=None):
        # prepare dict
        payload = self.prepare_order_dict(payment_dict, request, ip)

        if not 'method' in payload:
            payload['method'] = 'NewOrder'

        log = CrmLogs.objects.create(command_type=payload.get('method'), send_text=payload)
        result = tasks.call.delay(payload, 'transact', log_id=log.id)

        # save id
        # if result['is_error'] is False:
        #     order.api_id = result['res'].get('orderId')
        #     order.save()
        return log.id

    def new_order_card_on_file(self, main_order_id, product_id, price=None, **order_data):
        payload = {
            'method': 'NewOrderCardOnFile',
            'billingSameAsShipping': 'YES',
            'productId': str(product_id),
            'shippingId': 1,
            'previousOrderId': main_order_id,
            'preserve_force_gateway': 1,
        }

        if price:
            # set price
            payload['dynamic_product_price_' + str(product_id)] = price

        result = self.call(payload, 'transact')
        return result

    def new_order_card_on_file_async(self, main_order_id, product_id, price=None, **order_data):
        payload = {
            'method': 'NewOrderCardOnFile',
            'billingSameAsShipping': 'YES',
            'productId': str(product_id),
            'shippingId': 1,
            'previousOrderId': main_order_id,
            'preserve_force_gateway': 1,
        }

        if price:
            # set price
            payload['dynamic_product_price_' + str(product_id)] = price

        result = tasks.call.delay(payload, 'transact').id
        return result

    def get_products(self):
        payload = {'method': 'campaign_view', 'campaign_id': self.crm.campaign}
        result = self.call(payload, 'membership')
        if result['is_error'] is False:
            products = list(
                zip(result['res'].get('product_id', '').split(','), result['res'].get('product_name', '').split(',')))
            return products
        return []

    def get_product_info(self, product_id):
        payload = {'method': 'product_index', 'product_id': product_id}
        result = self.call(payload, 'membership')
        res = result['res']
        product_dict = {
            'name': res.get('product_name'),
            'is_visible': True,
            'quantity': 1,
            'price': res.get('product_price'),
            'api_id': product_id,
        }
        pprint(product_dict)

        return product_dict
