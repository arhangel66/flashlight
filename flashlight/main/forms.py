from calendar import monthrange

from crispy_forms.helper import FormHelper
from datetime import date
from django import forms

class CreditCardField(forms.IntegerField):
    def get_cc_type(self, number):
        """
        Gets credit card type given number. Based on values from Wikipedia page
        "Credit card number".
        <a href="http://en.wikipedia.org/w/index.php?title=Credit_card_number
    ">http://en.wikipedia.org/w/index.php?title=Credit_card_number
    </a>    """
        number = str(number)
        # group checking by ascending length of number
        if len(number) == 13:
            if number[0] == "4":
                return "visa"
        elif len(number) == 14:
            if number[:2] == "36":
                return "master"
        elif len(number) == 15:
            if number[:2] in ("34", "37"):
                return "American Express"
        elif len(number) == 16:
            if number[:4] == "6011":
                return "discover"
            if number[:2] in ("51", "52", "53", "54", "55"):
                return "master"
            if number[0] == "4":
                return "visa"
        return "Unknown"

    def clean(self, value):
        """Check if given CC number is valid and one of the
        card types we accept"""
        if value and (len(value) < 13 or len(value) > 16):
            raise forms.ValidationError("Please enter in a valid " + \
                                        "credit card number.")
        elif self.get_cc_type(value) not in ("visa", "master", "discover"):

            raise forms.ValidationError("Please enter in a Visa, " + \
                                        "Master Card, or Discover credit card number.")

        return super(CreditCardField, self).clean(value)


class PaymentForm(forms.Form):
    number = CreditCardField(required=True, label="Card Number")
    expire_month = forms.ChoiceField(required=True, choices=[(x, x) for x in xrange(1, 13)])
    expire_year = forms.ChoiceField(required=True,
                                    choices=[(x, x) for x in xrange(date.today().year, date.today().year + 15)])
    cvv_number = forms.IntegerField(required=True, label="CVV Number",
                                    max_value=9999, widget=forms.TextInput(attrs={'size': '4'}))


    def clean(self):
        cleaned_data = super(PaymentForm, self).clean()
        expire_month = cleaned_data.get('expire_month')
        expire_year = cleaned_data.get('expire_year')

        if expire_year in forms.fields.EMPTY_VALUES:
            # raise forms.ValidationError("You must select a valid Expiration year.")
            self._errors["expire_year"] = self.error_class(["You must select a valid Expiration year."])
            del cleaned_data["expire_year"]
        if expire_month in forms.fields.EMPTY_VALUES:
            # raise forms.ValidationError("You must select a valid Expiration month.")
            self._errors["expire_month"] = self.error_class(["You must select a valid Expiration month."])
            del cleaned_data["expire_month"]
        year = int(expire_year)
        month = int(expire_month)
        # find last day of the month
        day = monthrange(year, month)[1]
        expire = date(year, month, day)
        print(112, CreditCardField().get_cc_type(cleaned_data.get('number')))
        if date.today() > expire:
            # raise forms.ValidationError("The expiration date you entered is in the past.")
            self._errors["expire_year"] = self.error_class(["The expiration date you entered is in the past."])

        return cleaned_data


    def send(self, order):
        cd = self.cleaned_data
        cd['type'] = CreditCardField().get_cc_type(cd.get('number'))
        pprint(cd)
        crm = order.get_crm()
        api = crm.get_worker()
        api.send_order(order, cd)
        return True

