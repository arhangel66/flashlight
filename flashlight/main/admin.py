from codemirror2.widgets import CodeMirrorEditor
from django import forms
from django.contrib import admin
from django.contrib.flatpages.admin import FlatPageAdmin
from django.contrib.flatpages.models import FlatPage
from django.utils.translation import ugettext_lazy as _


class FlatPageForm(forms.ModelForm):
    content = forms.CharField(widget=CodeMirrorEditor(options={
        'lineNumbers': True,
        'mode': "django",
        'indentUnit': 2,
        'indentWithTabs': True,
        'theme': "mdn-like"
    }))

    class Meta:
        fields = '__all__'
        model = FlatPage


# Define a new FlatPageAdmin
class FlatPageCustAdmin(FlatPageAdmin):
    fieldsets = (
        (None, {'fields': ('url', 'title', 'content', 'sites')}),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': (
                'enable_comments',
                'registration_required',
                'template_name',
            ),
        }),
    )
    form = FlatPageForm


# Re-register FlatPageAdmin
admin.site.unregister(FlatPage)
admin.site.register(FlatPage, FlatPageCustAdmin)
