from pprint import pprint

from django.conf import settings
from django.contrib.auth.mixins import AccessMixin
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView, FormView, View
from django.contrib import messages
from flashlight.limelight.api import LimeLightApi
from flashlight.limelight.models import CrmLogs

flashlight_id = 235


class HasOrderMixin(AccessMixin):
    """
    View mixin which verifies that the user is authenticated.
    """
    pass

    def dispatch(self, request, *args, **kwargs):
        if not (request.session.get('main_order_id') or request.session.get('post_order') or request.session.get('main_order_log_id')):
            return HttpResponseRedirect(reverse('order'))

        return super(HasOrderMixin, self).dispatch(
            request, *args, **kwargs)


class UpsellMixin(View):
    actions = {'yes': {'id': '236', 'name': 'Premium Lithium Ion Rechargeable Battery'}}
    next = '/warranty/'

    def get(self, request, *args, **kwargs):
        key = request.GET.get('action')
        print(35, key, request.session.get('main_order_id'))
        if key and key in self.actions:
            product = self.actions.get(key)
            api = LimeLightApi()

            # check result of main async order
            if request.session.get('main_order_log_id'):
                log = CrmLogs.objects.get(id=self.request.session.pop('main_order_log_id'))
                if log.is_finish():
                    if log.is_error:
                        messages.add_message(self.request, messages.INFO, log.get_error_text())
                        return HttpResponseRedirect(reverse('order'))
                    else:
                        self.request.session['main_order_id'] = log.get_order_id()

            # res = api.new_order_card_on_file(self.request.session['main_order_id'], product['id'],
            #                                  price=product['price'])
            res = api.new_order_card_on_file_async(self.request.session['main_order_id'], product['id'],
                                 price=product['price'])

            if res:
                # print(39, {'total': res['res'].get('orderTotal'), 'product': product})
                # self.request.session['order'] += [{'total': res['res'].get('orderTotal'), 'product': product}]
                self.request.session['order'] += [{'total': product['price'], 'product': product}]
                print(41)
                pprint(self.request.session['order'])
                print(42)
                return HttpResponseRedirect(self.next)

        return super(UpsellMixin, self).get(request, *args, **kwargs)


class OrderView(TemplateView):
    template_name = 'pages/order.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['flashlight_id'] = flashlight_id
        return context

    def post(self, request, *args, **kwargs):
        post = request.POST
        # api = LimeLightApi()
        product_key = post.get('purchase[product_id]').split('_')[1]
        self.request.session['post_order'] = post
        self.request.session['post_product'] = settings.PRODUCTS.get(product_key)
        # res = api.send_order(post, request=request)
        # if res.get('is_error') is False:
        #     self.request.session['main_order_id'] = res['res'].get('orderId')
        #     self.request.session['order'] = [{'product': res.get('product'), 'total': res['res'].get('orderTotal')}]
        #     pprint(self.request.session)
        return HttpResponseRedirect(reverse('batteries'))


class BattariesView(HasOrderMixin, TemplateView):
    actions = {
        'yes': {'id': '236', 'name': 'Lithium Ion Rechargeable Battery', 'price': 9.99},
        'no': None
    }
    next = '/car_charger/'

    def get(self, request, *args, **kwargs):
        key = request.GET.get('action')
        pprint(self.request.session.keys())

        # if it is dobleclick to same link then we redirect to next page(if LL get the order) or back to /order/
        if not 'post_order' in self.request.session:
            if 'order' in self.request.session:
                return HttpResponseRedirect(self.next)
            else:
                return HttpResponseRedirect(reverse('order'))

        # pprint(self.request.session['post_order'])
        if key and key in self.actions:
            upsell_product = self.actions.get(key)
            api = LimeLightApi()
            post_dict = self.request.session.pop('post_order')
            pprint(post_dict)
            if upsell_product:
                post_dict['extra_product'] = upsell_product
            # if product:
            #     post_dict['upsellProductIds'] = product['id']
            #     post_dict['upsellCount'] = 1
            res = api.send_order_async(post_dict, request=request)
            # pprint(res)
            if res:
                self.request.session['main_order_log_id'] = res
                # self.request.session['main_order_id'] = res['res'].get('orderId')
                main_product = self.request.session.pop('post_product')
                self.request.session['order'] = [
                    {'product': main_product, 'total': main_product['price'] * int(main_product['qty'])}]
                if upsell_product:
                    self.request.session['order'] += [{'total': upsell_product['price'], 'product': upsell_product}]
                pprint(self.request.session['order'])
            # else:
            #     from django.contrib import messages
            #     message = res['res'].get('errorMessage') if 'errorMessage' in res['res'] else str(res['res'])
            #     messages.add_message(request, messages.INFO, message)
            #     return HttpResponseRedirect(reverse('order'))

            return HttpResponseRedirect(self.next)

        return super(BattariesView, self).get(request, *args, **kwargs)


class WarrantyView(HasOrderMixin, UpsellMixin, TemplateView):
    actions = {
        'yes1': {'id': '237', 'name': '1 Year Warranty', 'price': 15},
        'yes3': {'id': '238', 'name': '3 Year Warranty', 'price': 20}
    }
    next = '/thankyou/'


class CarChargerView(HasOrderMixin, UpsellMixin, TemplateView):
    actions = {
        'yes': {'id': '248', 'name': 'Car Charger', 'price': 14.99}
    }
    next = '/warranty/'


class ThankYouView(HasOrderMixin, TemplateView):
    def get(self, request, *args, **kwargs):
        if self.request.session.get('main_order_log_id'):
            log = CrmLogs.objects.get(id=self.request.session.pop('main_order_log_id'))
            print(169, log.is_error, log.get_error_text())
            if log.is_finish():
                log = CrmLogs.objects.get(id=log.id)  # update data from db
                if log.is_error:
                    from django.contrib import messages
                    messages.add_message(self.request, messages.INFO, log.get_error_text())
                    print(176, 'here')
                    return HttpResponseRedirect(reverse('order'))
                else:
                    self.request.session['main_order_id'] = log.get_order_id()
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['flashlight_id'] = flashlight_id

        context['total'] = self.get_total(self.request.session.get('order', []))
        context['order'] = self.request.session.get('order')
        # self.request.session
        if 'main_order_id' in self.request.session:
            self.request.session.pop('main_order_id')

        # check result of main async order
        return context

    def get_total(self, order):
        total = 0
        for line in order:
            if line and type(line) == type({}) and line.get('total'):
                total += float(line.get('total'))
        return total
